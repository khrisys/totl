package fr.example.khris.jpaspringboot;

import fr.example.khris.jpaspringboot.dao.ProduitDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * SPRING BOOT
 * 1 - GAIN DE TEMPS DE CONSTRUCTION DE PROJET AVEC SPRING BOOT AVEC GESTION DES DEPENDANCES JUSTE EN COCHANT CE QU'ON A BESOIN
 * DANS LE PROJET, C'EST SPRING BOOT QUI VA CHERCHER TOUTES LES DEPENDANCES VOULUES.
 * UTILISATION DE SPRINGINITIALIZR
 * 2 - AUTOCONFIGURATION PAR SPRING BOOT VIA LE FICHIER APPLICATION.PROPERTIES
 *
 * SPRING DATA :
 * * - OPTIMISATION DES TEMPS DE DEV, GRACE AUX ANNOTATIONS, INTERFACE JpaRepository QUI A DEJA ECRIT LA PLUPART DES
 * INTERFACES GENERIQUES DU CRUD, ECRITURE DES METHODES PLUS PERSONNALIS2ES AVEC AUTOCOMPLETION, PAS DE CLASSES A ECRIRE
 * * - PAS D'ERREUR DE FRAPPE HUMAINE : LE CODE EST GENERE ET NE COMPORTE PAS DE FAUTE
 * * - l'IoC PERMET DE NE PLUS ECRIRE DE CLASSE POUR GERER LE CRUD ET AUTRES METHODES SPECIFIQUES : L'INTERFACE
 *  JPAREPOSITORY LE PERMET DIRECTEMENT? ET INJECTE CES METHODES LA OU ON VEUT DANS LE CODE
 *
 * 1 - ECRIRE  LES MODELS AVEC ANNOTATIONS NE COMPORTANT D'AILLEURS QUE LES ATTRS
 * 2 - ECRIRE INTERFACE ProduitDao extends JpaRepository<type de l'objet, type de l'id>,
 * 3 - ECRIRE DES METHODES PLUS SPECIFIQUES BASEES SUR L'AUTOCOMPLETION DANS L'INTERFACE (IL N'Y A PAS DE ClASSES!!!!); CA
 * IMPLIQUE QU'ON NE PEUT PAS ECRIRE N'IMPORTE QUELLE METHODE LORSQU'ON UTILISE L'AUTOCOMPLETION, MAIS DES METHODES SUR LES ATTRS
 * DEFINIS DANS LE MODEL
 * 4 - ON PEUT AUSSI ECRIRE DES METHODES COMPLETEMENT PERSONNALIS2ES 5oN LEUR DONNE LE NOM QU'ON VEUT), LA, IL FAUT ECRIRE LA
 * REQUETE
 *
 *
 */
@SpringBootApplication
public class JpaspringbootApplication {
    
    @Autowired
    ProduitDao produitDao;
    
    public static void main(String[] args) {
        
        // C'est Spring Boot qui demarre en 1ere dans l'appli, et il va mettre en place l'IoC +
        // 1 - il lit le fichier de conf properties +
        // 2 - comme il sait qu'on utilise JPA et mysql, automatiquement au demarrage, Spring va creer un objet
        // entitymanagerFactiry +
        // 3 - il lit les données sur le datasource +
        // 4 - il voit que la bdd n'existe pas alors il la cree depuis les models
        SpringApplication.run(JpaspringbootApplication.class, args);
    }

}
