package fr.example.khris.jpaspringboot.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
public class Produit implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    
    private String description;
    private double prix;
    private int quantite;
    
    //Constructeur sans param obligatoire à cause de l'interface Serializable
    public Produit() {
    }
    
    //===============================GETTERS & SETTERS =======================================
    
    public int getId() {
        return id;
    }
    
    public void setId(int pId) {
        id = pId;
    }
    
    public String getDescription() {
        return description;
    }
    
    public void setDescription(String pDescription) {
        description = pDescription;
    }
    
    public double getPrix() {
        return prix;
    }
    
    public void setPrix(double pPrix) {
        prix = pPrix;
    }
    
    public int getQuantite() {
        return quantite;
    }
    
    public void setQuantite(int pQuantite) {
        quantite = pQuantite;
    }
}
