package fr.example.khris.jpaspringboot.dao;

import java.util.List;

/**
 * Interface générique pour utilisation des memes methodes pour toutes les entités de l'application
 * En fait, on en a pas besoin car la classe abstraite JpaRepository de Spring Data gère tout à la place.
 *
 * @param <U> Type U representant  une Entité (classes d u package model)
 * @param <V> Type V  representant l'id de l'entité U
 */
public interface IEntityDao<U, V> {
    
    public U save(U obj);
    public List<U> findAll();
    public U find(V id);
    public void remove(V id);
    public  void update(U obj);
    
}
