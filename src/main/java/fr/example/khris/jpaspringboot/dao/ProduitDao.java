package fr.example.khris.jpaspringboot.dao;

import fr.example.khris.jpaspringboot.model.Produit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * La classe abstraite générique JpaRepository comporte toutes les classes du CRUD qu'on a l'habitude d'utiliser. Il n'y a meme
 * pas besoin de creer une interface généraique par nous meme, Spring Data l'a dejà fait!
 * Comme les methodes ont dejà été dedefinies dans Spring Data, cette interface est vide si on utilise uniquement les methodes
 * basiques du CRUD. C'est normal. Il faudra par contre utiliser @Autowire sur les classes qui utilisent ces methodes du CRUD
 * (pour l'injection de dependance)
 *
 * Il n'y  a meme plus besoin d'ecrire de classe!!!
 */
@Repository
public interface ProduitDao extends JpaRepository<Produit, Integer> {
    
    /**
     * Methode plus specifique (pas seulement le CRUD classique) permettant de retourner une liste de produit contenant une
     * description. La maniere de generer ces methodes est definie selon Spring Data par des mots clefs qui permettent d'ecrire
     * la plupart des methodes. Utiliser l'autocompletion!
     * Grace à ca, on n'a meme pas besoin d'ecrire la requete HQL. On ne peut donc pas faire d'erreur!
     *
     * @param description
     * @return liste de produits
     */
    public List<Produit> findByDescriptionContains(String description);
    
    /**
     * Methode completement personnalisée qui ne peut pas utiliser les avantages pricipaux de Spring Data. On est obligé
     * d'ecrire la requete, mais necessite quelques annotations
     * Ici, on cherche à retourner une liste de produits avec un mot clef defini et dont le prix mini est de "x"
     *
     * @param mc
     * @param prixMin
     * @return
     */
    @Query("SELECT p from Produit p WHERE  p.description like :x and p.prix > :y")
    public List<Produit> chercherProduits(@Param("x")String mc, @Param("y")double prixMin);
    
}
